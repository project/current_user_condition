# Current User Profile Condition

Provides condition plugin for the current authenticated users profile page. Also known as the user canonical page.
Compatible with block visibility and any other component utilizing condition plugins.

## Usage

1. Download and install the `drupal/current_user_condition` module. Recommended install method is composer:
   ```
   composer require drupal/current_user_condition
   ```
2. Check the "Is the current authenticated users profile page" option as needed.
